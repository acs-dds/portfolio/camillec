import React from "react";

import Header from "./Header";
import Question from "./Body/Question";
import '../css/style_n.css';
import '../App.css';

export default class Layout extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			data : "",
			question: "",
			reponse: "",
			propositions: [],
			indice: 0,
			score: 0
		};

		fetch('http://nabilb.dijon.codeur.online:11001/', {
		    method: 'GET',
		    credentials: 'include',
		    headers: {
		    	'Origin' : 'http://nabilb.dijon.codeur.online:3001/',
		    	/*'Access-Control-Request-Method': 'GET',*/
		    }
		})
		.then((res) => {
			return res.json();
		})
		.then((res) => {
			this.setState({data : res});
			this.setState({question : this.state.data[this.state.indice].question});
			this.setState({propositions: this.state.data[this.state.indice].propositions});
		})
		.catch(function (error) {  
		  console.log(error);
		});

		this.checkRep = this.checkRep.bind(this);

	}

	checkRep() {

		if (this.state.indice <= 18) {
			var form = new FormData();
			form.append("question", this.state.indice);
			form.append("reponse", document.querySelector('input[name="choice"]:checked').value);

			fetch('http://nabilb.dijon.codeur.online:11001/check', {
			    method: 'POST',
			    credentials: 'include',
			    headers: {
			    	'Origin' : 'http://nabilb.dijon.codeur.online:3001/',
			    },
			  	body: form
			})
			.then((res) => {
				return res.json();
			})
			.then((res) => {
			  if (res[0].resultat === "True") {

			  	var rep_ok  = document.querySelector('input[name="choice"]:checked').parentNode;
				rep_ok.classList.add("ok");

			  	setTimeout(() => {
			  		this.setState({indice : this.state.indice + 1});
			  		this.setState({question : this.state.data[this.state.indice].question});
					this.setState({propositions: this.state.data[this.state.indice].propositions});
					rep_ok.classList.remove("ok");
					document.querySelector('input:checked').checked = false;
			  	}, 1000);

			  	this.setState({score: res[0].score});
			  }	else {
				  	var rep_nok  = document.querySelector('input[name="choice"]:checked').parentNode;
				  	rep_nok.classList.add("nok");

				  	setTimeout(() => {
			  		this.setState({indice : this.state.indice + 1});
			  		this.setState({question : this.state.data[this.state.indice].question});
					this.setState({propositions: this.state.data[this.state.indice].propositions});
					rep_nok.classList.remove("nok");
					document.querySelector('input:checked').checked = false;
			  	}, 1000);
			  	}
			})	
			.catch(function (error) {  
			  console.log("nok", error);
			});
		} else {
			var end = document.createElement("h3");
			var txt = document.createTextNode("Partie terminée !!");
			/*var btn = document.createElement("input");
			btn.type = "submit";
			btn.value = "Rejouer";
			btn.classList.add("check");*/

			end.appendChild(txt);
			/*document.getElementById("ask").appendChild(btn)*/

			var q = document.getElementById("ask").firstChild;

			document.getElementById("ask").replaceChild(end,q);
			setTimeout(() => {
				window.location.reload();
			}, 2000);
			
		}
	}

	render() {
		return (
			<div>
				<Header />
				<Question reponse={this.state.reponse} question={this.state.question} propositions={this.state.propositions} checkRep={this.checkRep} score={this.state.score}/>
			</div>
		);
	}
}