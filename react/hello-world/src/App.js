import React, { Component } from 'react';
import './App.css';

export default class App extends Component {
  render() {
    return (
      <div className="App">
          <h2>Hello World !</h2>
        <p className="App-intro">C'est du React !</p>
      </div>
    );
  }
}


