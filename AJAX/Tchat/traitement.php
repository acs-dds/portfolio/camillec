<?php

require 'classe/classController.php';

// si l'information renseignée à plus de zéro caractère
if (count($_GET) > 0) {
	// on lance le controleur
	$c = new Controller();
	// isset détermine si une valeur est définie. 
	if (isset($_GET['nom'])){
		echo $c->register($_GET['nom']);
	}
	if (isset($_GET['message'])){
		echo $c->postMessage($_GET['message']);
	}
	if (isset($_GET['channel'])){
		echo $c->setDiscussion($_GET['channel']);
	}
	if (isset($_GET['get'])) {
		if (isset($_GET['new'])) {
			echo $c->getNewMessage();
		}else{
			echo $c->getHistory();
		}
	}
}



// header('Location: accueil.php');

// if (empty($_POST['nom'])){
// 	header('Location: tchat.php');
// }


// $fichier = fopen ("data/nom.csv", 'a');

// fputcsv($fichier, array($_POST['nom'],"t"), ',');

// fclose($fichier);

?>