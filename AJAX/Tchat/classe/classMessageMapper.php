<?php

require_once 'classMessage.php';

class MessageMapper {
	private $fichier;

// dans la function contruct, on lui donne un nom pour donner un nom au general.
	public function __construct($discussion = 'general') {
		// si le doc n'existe pas (ici) alors créer le fichier
		if(!file_exists($this->fichier = __DIR__ . '/../discussions/' . $discussion)){
			touch($this->fichier);
		}
	}

// ajouter un message dans le fichier
	public function addMessage($message) {
		// ouvre le fichier, en mode "a" -> on ajoute à la fin
		$f = fopen($this->fichier, "a");
		//dans le fichier, on configure le message comme dans la fonction toArray
		fputcsv($f, $message->toArray());
		fclose($f);
	}

	public function getMessages($depuis = 0) {
		$f = fopen($this->fichier, "r");
		$msgs = [];
		while ($ligne = fgetcsv($f)) {
			if ($ligne[1] < $depuis) continue; // si la date du message est inférieure au $depuis, on ne veut pas du Message
			$msgs[] = new Message($ligne[0], $ligne[1], $ligne[2]);
		}
		return $msgs;
	}
}

?>