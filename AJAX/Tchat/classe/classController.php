<?php

require_once 'classMessageMapper.php';

class Controller {
	private $mapper;

	public function __construct(){
		session_start();
		setlocale(LC_ALL, 'fr_FR');
		if (isset($_SESSION['discussion'])){
			$this->mapper = new MessageMapper($_SESSION['discussion']);
		} else {
			$this->mapper = new MessageMapper();
		}
	}

	public function setDiscussion($channel) {
		$_SESSION['discussion'] = $channel;
	}

	public function isRegistered(){
		return isset($_SESSION['nom']);
	}

	public function getHistory(){
		if (!$this->isRegistered()) return 2;
		$messages = $this->mapper->getMessages();
		$html = "";
		foreach ($messages as $message) {
			$html .= "<li>" . $message->renderHtml() . "</li>";
		}
		// microtime pour récupérer un temps précis
		$_SESSION['last_update'] = microtime(true);
		return $html;
	}

// function getNewMessage est presque identique à getHistory
	public function getNewMessage(){
		if (!$this->isRegistered()) return 2;
		$messages = $this->mapper->getMessages($_SESSION['last_update']);
		$html = "";
		foreach ($messages as $message) {
			$html .= "<li>" . $message->renderHtml() . "</li>";
		}
		// microtime pour récupérer un temps précis
		$_SESSION['last_update'] = microtime(true);
		return $html;
	}

	public function register($nom){
		$_SESSION['nom'] = $nom;
		return 0;
	}

	public function postMessage($message){
		if (!$this->isRegistered()) return 2;
		$this->mapper->addMessage(new Message($_SESSION['nom'], microtime(true), $message));
	}
}

?>