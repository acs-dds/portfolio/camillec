<?php

class Message {
	private $author;
	private $date;
	private $content;

	public function __construct($author, $date, $content){
		$this->author = $author;
		$this->date = $date;
		$this->content = $content;
	}

	public function toArray(){
		return[$this->author, $this->date, $this->content];
	}

	public function renderHTML(){
		return "<h2>{$this->author}</h2> <p>{$this->content}</p> <time>" . strftime("%A %e %B %H:%M:%S", $this->date) ."</time>";
		//output buffer
		// ob_start();
		// require("");
		// return ob_get_clean();
	}

	// retourner avec une fonction magique
}

?>