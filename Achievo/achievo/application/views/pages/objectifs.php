<form method="post" action="<?php echo site_url('logout'); ?>">
	<input type="submit" value="Déconnexion">
</form>
<?php


// foreach : première variable un tableau / deuxième variable la valeur de l'élément courant.
// ici on passe un tableau $objs (cf controller) et la valeur de l'élément $objs en court est assignée à $obj

	foreach ($objs as $obj): ?>
	<article>
<!-- chaque élément $obj sera retourné avec le var_dump -->
		<?php var_dump($obj); ?>
		<a href="<?php echo site_url('view/' . $obj['id']); ?>">Objectif</a>
	</article>
	<?php endforeach; ?>