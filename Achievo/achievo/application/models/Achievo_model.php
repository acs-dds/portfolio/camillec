<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Achievo_model extends CI_Model {

	private $idutilisateur;

	public function __construct(){
		$this->load->database();
	}

	public function setUtilisateur($idutilisateur) {
		$this->idutilisateur = $idutilisateur;
	}

	public function utilisateur(){
		return isset($this->idutilisateur);
	}

	public function fetch_objs() {

		if($this->utilisateur()) {
// dans la base de données sélectionnée on sélectionne les données voulues
			$select = "SELECT id, titre, intitule, objectif, progression FROM succes WHERE idutilisateur = ? ORDER BY id ASC";
// la fonction query retourne un résultat de la base de données.
			$result = $this->db->query($select, [$this->idutilisateur]);

			var_dump($this->idutilisateur);

// pour afficher les résultats on utilise la fontion result_array. result_array affiche les données de $resultat
			return $result->result_array();			
		}
	}

	public function fetch_obj($id) {

		var_dump($this->idutilisateur);
/*
		if($this->utilisateur()) {*/
// faire une requête préparée = sécurité. lorsqu'on sélectionne la table, l'élément qui changera à chaque requete, ici l'id, est égal à un ?. lorsqu'on query on lui passe la variable $select avec l'arguemnt $id
		$select = "SELECT id, titre, intitule, objectif, progression FROM succes WHERE id = ?";

		$result = $this->db->query($select,[$id]);

		return $result->result_array();			
		/*}*/
	}

	public function maj($progression, $id) {

		/*if($this->utilisateur()) {*/
		$select = "UPDATE succes SET progression = ? WHERE id = ? AND idutilisateur = ?";
		$result = $this->db->query($select, [$progression, $id, $this->idutilisateur]);			
		/*}*/
	}
}