<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utilisateur_model extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	public function login($login, $mdp) {
		$select = "SELECT * FROM utilisateur WHERE entreprise = ? AND mdp = ?";

		$result = $this->db->query($select, [$login, hash("sha512", "ok@/" . $mdp)]);

		return $result->row();
	}

	public function getId($user) {
		$select = "SELECT * FROM utilisateur WHERE entreprise = ?";
		$result = $this->db->query($select, [$user]);

		return $result->row()->id;
	}

}