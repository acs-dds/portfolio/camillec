<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Achievo extends CI_Controller {

	public function __construct(){

// parent::__construct -> pour charger le __construct par défaut de CI
		parent::__construct();
//  helper 'url' c'est pour charger les aide de CI pour faciliter l'utilisation des urls (site_url)
		$this->load->library(array("session","form_validation"));
		$this->load->helper(array("url","form"));

		$this->load->model('utilisateur_model');
		$this->output->enable_profiler(true);
	}

	public function allObjs() {

// s'il n'y a pas de session utlisateur alors on redirige vers le controleur par défaut (page register)
		// if(!$this->session->has_userdata('utilisateur')) {
		// 	redirect(site_url(''));
		// }

// on charge les models archievo_model et utilisateur_model
		$this->load->model('utilisateur_model');

// dans le model achievo on charge la méthode setUtilisateur. Il a besoin du model utilisateur et de la méthode getIT dans laquelle on récupère la donnée du l'iput login
		$this->achievo_model->setUtilisateur($this->utilisateur_model->getId($this->input->post('login')));

// dans la variable $info je stocke la méthode fetch_obj du model archievo
		$info = $this->achievo_model->fetch_objs();

// dans la variable $data on met les données contenu dans le model ????
		$data = array(
			'objs' => $info
		);

// on charge les vues
		$this->load->view('common/header');
// on charge la vue objectifs et on lui passe la variable $data pour afficher les données
		$this->load->view('pages/objectifs', $data);
		$this->load->view('common/footer');		
	}

	public function view($id) {

// s'il n'y a pas de session utlisateur alors on redirige vers le controleur par défaut (page register)
		// if(!$this->session->has_userdata('utilisateur')) {
		// 	redirect(site_url(''));
		// }

// on charge le model archievo_model

// dans la variable $info je stocke la méthode fetch_obj du model archievo
		$info = $this->achievo_model->fetch_obj($id);
		var_dump($info);

// dans la variable $data on met les données contenu dans le model ????
		$data = array(
			'objs' => $info
		);

// on charge les vues
		$this->load->view('common/header');
// on charge la vue objectif et on lui passe la variable $data pour afficher les données
		$this->load->view('pages/objectif', $data);
		$this->load->view('common/footer');
	}

	public function update($id) {

		$progression = $this->input->post('progression');
		$this->achievo_model->maj($progression, $id);

	}

	public function index() {
// on utilise la validation des formulaires. set_rules pour définir les règles de validation
		$this->form_validation->set_rules('user', 'User',"required");
  		$this->form_validation->set_rules('password', 'Password',"required");

// si le formulaire n'est pas validé, on retourne à la page register
  		if($this->form_validation->run() == false){
  			$this->load->view('common/header');
			$this->load->view('register');
			$this->load->view('common/footer');
			var_dump($this->form_validation->run());
		} else {
			echo "test";
			exit;
			/*// dans la variable $log on charge la méthode login. on récupère les données de login et de mot de passe dans la page register.
			$login = $this->input->post('login');
			$mdp = $this->input->post('mdp');

			// si $log est différent de vide alors on crée une session utilisateur pour l'utilisateur et on redirige vers la méthode allObjs
			var_dump($this->utilisateur_model->login($login,$mdp));

			if (!empty($this->utilisateur_model->login($login,$mdp))){
				$this->session->utilisateur = true;
			}*/
  		}
	}

	public function logout() {

// on ferme la session utilisateur et on détruit la session
		$this->session->unset_userdata('utilisateur');
		$this->session->sess_destroy();
		redirect('');
	}
}