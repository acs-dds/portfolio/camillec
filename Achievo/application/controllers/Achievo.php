<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Achievo extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('achievo_model');
		$this->output->enable_profiler(true);
	}

	public function index() {

		$info = $this->achievo_model->fechtObjs();

		$data = array(
			'objs' =>$info
		);

		$this->load->view('pages/objectifs', $data);
	}

	public function view($id) {
		$info = $this->achievo_model->fechtObj($id);

		$data = array(
			'objs' =>$info
		);

		$this->load->view('common/header');
		$this->load->view('pages/objectif', $data);
		$this->load->view('common/footer');
	}

	public function update($id) {

		$this->achievo_model->maj($id);
		$progression = $this->input->post('progression');

	}

}