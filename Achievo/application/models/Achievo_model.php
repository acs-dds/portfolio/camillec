<?php

class Achievo_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function fechtObjs() {

		$select = $this->db->query("SELECT * FROM succes ORDER BY id ASC");
		return $select->result_array();
	}

	public function fechtObj($id) {

		$select = $this->db->query("SELECT * FROM succes WHERE id = ?;", [$id]);
		return $select->result_array();
	}
	public function maj($progression, $id) {

		$select = $this->db->query("UPDATE succes SET progression = ? WHERE id = ?;", [$progression, $id]);
	}
}