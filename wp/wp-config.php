<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'camillec');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'camillec');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'B3qwG4f9');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sW>/>+gNdUwlf$L!}2<j|6 Ji=]A6|,:1fg1T1uqb9 cL*AfpL0;6`~fY#}OkidT');
define('SECURE_AUTH_KEY',  '>g0M_CnH.uwsp*IH=7FJBHuYiZ:TVkHR$1 )51c|n,$oxh^=}$.0t5BFgW 3U?<<');
define('LOGGED_IN_KEY',    '$-/Hdlu*Q15;aQ&ta>K3z.PD(T^<SJe^w5zf@5QzDc>BX :*` Lcmb}3m&MMfG+z');
define('NONCE_KEY',        'k$kA*ByY~Rq;22tUt^oN9^,>jo6j#f<d)p9M!T`%E> HY~:_.Vl{)N,9q:;wD6Hh');
define('AUTH_SALT',        'P}XRknS8Q>}.Z)fU9qrXg&oArdl^cN FN5,IN[=h^}kp/p-$F[UD|JY3n`<tP)`t');
define('SECURE_AUTH_SALT', 'H&xQGZ_5jMMHCCk~.<d;TZcl8{BRNJ)D}AJV<~k8$.B]ZZ+K.0iLB*6[&AD^gfKT');
define('LOGGED_IN_SALT',   'd9ML^PFj7%EA:*JB%z&scf=Nt$`GJ&6zu)cXb,8+9I7a/nH)Z^Zq,RXl&jGI%;Fg');
define('NONCE_SALT',       '8~;)dC.!XJ<dH>{ea{&4N vP2N1$Fz}j#)`JJtA+G@,i!4`!<]#U,6+<mee8xlk7');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_5sR9YA';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');