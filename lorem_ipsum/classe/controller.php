<?php
require_once 'mapper.php';

class Controller {
	private $mapper;

	public function __construct(){
		$this->mapper = new Mapper();
	}
	
	// public function genererText($nb) {
	// 	return $this->mapper->genererMot($nb);
	// }

	public function genererNbParagraphe($nb, $nbParagraphe) {
		return $this->mapper->genererPara($nb, $nbParagraphe);
	}

	public function loadThemeAction($theme){
		return $this->mapper->loadTheme($theme);
	}

}

?>