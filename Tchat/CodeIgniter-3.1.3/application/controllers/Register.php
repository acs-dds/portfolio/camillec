<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function index() {

		$this->load->view('common/header');
		$this->load->view('pages/inscription');
		$this->load->view('common/footer');
	}

	public function create() {

		$this->load->view('common/header');
		$this->load->view('pages/inscription');
		$this->load->view('common/footer');

		$nom = $this->input->post('nom');
		$prenom = $this->input->post('prenom');
		$pseudo = $this->input->post('pseudo');
		$email = $this->input->post('email');
		$motDePasse = $this->input->post('mdp');

		$this->load->model('register_model');

		$this->register_model->set_register($nom, $prenom, $pseudo, $email, $motDePasse);

		$this->load->view('pages/success');
	}
}
