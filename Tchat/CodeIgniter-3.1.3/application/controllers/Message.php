<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

	public function index() {

		$this->load->model('message_model');
		$test = $this->message_model->get_message();

		$data = array(
			'infos' => $test
		);

		$this->load->view('pages/test', $data);

		$this->load->view('common/header');
		$this->load->view('pages/tchat');
		$this->load->view('common/footer');

	}

	public function get_message() {

		$this->load->model('message_model');
		$test = $this->message_model->get_message();

		$data = array(
			'infos' => $test
		);

		$this->load->view('pages/test', $data);

		$this->load->view('common/header');
		$this->load->view('pages/tchat');
		$this->load->view('common/footer');

		$message = $this->input->post('message');

		$this->message_model->add_message($message);

	}

}