<?php

class Message_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

	public function add_message($message) {
		$data = array(
			'contenu_message' => $message,
			'id_forum' => 1,
			'id_utilisateur' => 24
		);
	return $this->db->insert('message', $data);
	}

	public function get_message() {

		$query = "SELECT pseudo, date, contenu_message FROM utilisateur INNER JOIN message ON message.id_utilisateur = utilisateur.id ORDER BY date ASC";
		$result = $this->db->query($query);

		return $result->result_array();

	}
}

?>