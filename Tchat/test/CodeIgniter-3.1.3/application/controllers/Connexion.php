<?php
class Connexion extends CI_Controller {

        public function __construct()
        {

                parent::__construct();
                $this->load->model('news_model');
                $this->load->helper('url_helper');
                // $this->output->enable_profiler(true);
        }

        public function index()
        {
                $data['news'] = $this->news_model->get_news();
                $data['title'] = 'News archive';

        $this->load->view('templates/header', $data);
        $this->load->view('news/index', $data);
        $this->load->view('templates/footer');
        }

        public function view($slug = NULL)
        {
                $data['news_item'] = $this->news_model->get_news($slug);

                if (empty($data['news_item']))
        {
                show_404();
        }

        $data['title'] = $data['news_item']['title'];

        $this->load->view('templates/header', $data);
        $this->load->view('news/view', $data);
        $this->load->view('templates/footer');
        }

        public function verificate()
        {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create a news item';

        $this->form_validation->set_rules('nom', 'Nom', 'required');
        $this->form_validation->set_rules('prenom', 'Prenom', 'required');
        $this->form_validation->set_rules('pseudo', 'Pseudo', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('mdp', 'Mot de passe', 'required');

        if ($this->form_validation->run() === FALSE)
        {
        $this->load->view('templates/header', $data);
        $this->load->view('pages/inscription');
        $this->load->view('templates/footer');

        }
        else
        {
        $nom = $this->input->post('nom');
        $prenom = $this->input->post('prenom');
        $pseudo = $this->input->post('pseudo');
        $email = $this->input->post('email');
        $motDePasse = $this->input->post('mdp');
        $this->news_model->set_news($nom, $prenom, $pseudo, $email, $motDePasse);
        $this->load->view('news/success');
        }
        }
}