<?php
class Register_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

public function set_register($nom, $prenom, $pseudo, $email, $motDePasse)
{
    $this->load->helper('url');

    $slug = url_title($this->input->post('title'), 'dash', TRUE);

    $data = array(
        'nom' => $nom,
        'prenom' => $prenom,
        'pseudo' => $pseudo,
        'email' => $email,
        'password' => $motDePasse
    );

    return $this->db->insert('utilisateur', $data);
}
}