<?php
	//session_start crée une session ou restaure celle trouvée sur une serveur. elle retour true si une session a pu être démarrée avec succès, sinon false
	session_start();

	// array_map est une fonction sur les eléments d'un tableau
	// trim supprime espace
	$p = array_map('trim', $_POST);

	// var valide est par défaut vrai, elle se transformera en faux lorqu'il y aura des erreurs
	$valide = true;

	// un tableau erreurs créé pour toutes les stocker
	$erreurs = [];

if (strlen($p['prenom']) < 3) {
	$valide = false;
	$erreurs[] = 'Dommage, votre prénom n\'est pas assez long ! Il doit comporter minimum 3 caractères.';
}

if (strlen($p['nom']) < 3) {
	$valide = false;
	$erreurs[] = 'Dommage, votre nom n\'est pas assez long ! Il doit comporter minimum 3 caractères.';
}

if (preg_match("/[a-z0-9_.-]+@[a-z0-9-]{2,}/i", $p['email']) === 0) {
	$valide = false;
	$erreurs[] = 'L\'adresse mail n\'est pas valide.';
}

	// strpos recherche la première occurence dans le message
if (strlen($p['message']) < 20 || strpos($p['message'], ' ') === false) {
	$valide = false;
	$erreurs[] = 'Vous n avez pas écrit assez !';
}

if (!$valide) {
	$_SESSION['erreurs'] = $erreurs;
	header('Location: http://camillec.dijon.codeur.online/Portfolio/test/test_contact.php');
	exit;
}

if ($_POST['envoyeur'] == 'camille') {
    header('Location: http://camillec.dijon.codeur.online/Portfolio/test/test_portfolio.php?message='.$_POST['message'].'&nom='.$_POST['nom'].'&prenom='.$_POST['prenom']);
	exit;
}

if ($_POST['envoyeur'] == 'farid') {
    header('Location: http://faridl.dijon.codeur.online/ProjetFEC/portofolio.php?message='.$_POST['message'].'&nom='.$_POST['nom'].'&prenom='.$_POST['prenom']);
    exit;
}

if ($_POST['envoyeur'] == 'edouard') {
    header('Location: http://edouardb.dijon.codeur.online/PHP/portfolio.php?message='.$_POST['message'].'&nom='.$_POST['nom'].'&prenom='.$_POST['prenom']);
    exit;
}

?>