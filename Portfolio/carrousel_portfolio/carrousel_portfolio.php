<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Carrousel</title>
	<link rel="stylesheet" type="text/css" href="/Portfolio/carrousel_portfolio/carrousel_portfolio.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/Portfolio/carrousel_portfolio/carrousel_portfolio.js"></script>
</head>
<body>
<?php include "/home/faridl/ProjetFEC/navigation.html" ?>
	<figure class="image">
		<ul class="carrousel">
			<li><img src="/Portfolio/carrousel_portfolio/etoile.png" style="width: 435px; height: 244px;"/></li>
			<li><img src="/Portfolio/carrousel_portfolio/hoctogonale.png" style="width: 435px; height: 244px;"/></li>
			<li><img src="/Portfolio/carrousel_portfolio/simple.png" style="width: 435px; height: 244px;"/></li>
			<li><img src="/Portfolio/carrousel_portfolio/hoc2.png" style="width: 435px; height: 244px;"/></li>
		</ul>
	</figure>	
	<button id="previous" class="btn1 prev">Précédent</button>
	<button id="next" class="btn2 next">Suivant</button>

</body>
</html>