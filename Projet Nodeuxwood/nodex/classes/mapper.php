<?php

class Mapper {

	public function getClient($login) {
		$h = fopen('data/clients.csv', 'r');

		while ($ligne = fgetcsv($h, 0, ';')) {
			if ($ligne[2] == $login) return new Client($ligne);
		}
	}

	public function getCatalogue($typo) {
		$cat = [];

		$h = fopen('data/produits.csv', 'r');

		while ($ligne = fgetcsv($h, 0, ';')) {
			$typos = array_map('trim', explode(',', $ligne[5]));
			if (in_array($typo, $typos)) {
				$cat[] = new Produit($ligne);
			}
		}

		return $cat;
	}
}