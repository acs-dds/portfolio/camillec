<?php

session_start();

require 'classes/client.php';

$h = fopen('data/clients.csv', 'r');

while ($ligne = fgetcsv($h, 0, ';')) {
	if ($ligne[2] == $_POST['login'] && $ligne[3] == $_POST['mdp']) {
		$_SESSION['client'] = new Client($ligne);
		header('Location: accueil.php');
		exit;
	}
}

header('Location: index.php');
exit;