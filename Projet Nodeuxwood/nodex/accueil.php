<?php

require 'classes/client.php';
session_start();


if (!isset($_SESSION['client'])) {
	header('Location: index.php?bien_essaye');
	exit;
}

require 'classes/mapper.php';

Mapper::getCatalogue($_SESSION['client']->getTypologie());