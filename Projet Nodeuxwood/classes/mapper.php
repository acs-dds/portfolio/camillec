<?php

// on appel une seule fois
require_once '/home/camillec/Projet Nodeuxwood/données clients/clients.php';
require_once '/home/camillec/Projet Nodeuxwood/classes/catalogue.php';

class mapper {

	// l'identifiant et le mot de passe
	// dans la fonction, on met les arguments 'identifiant' et 'mot de passe' car c'est ce que l'on cherche.
	public function getClient($identifiant, $mdp) {
		// on ouvre le document dans lequel il y a les arguments qui nous intéresse
		$h = fopen ('nodex/clients.csv', 'r');

		// dans ce document, on fait une boucle pour comparer les informations
		// dans $ligne on analyse un fichier. le fichier est contenu dans $h, '0' pour dire qu'on prend tout le fichier (pas de limite) et le séparateur ';'
		while ($ligne = fgetcsv($h, 0, ';')) {
			// si l'identifiant situé à la 2ème colonne est égal à l'identifiant donnée par le client (idem pour le mot de passe) alors on stop le process et on crée un nouveau client.
			if ($ligne[2] == $identifiant && $ligne[3] == $mdp)
				return new Client($ligne);
		}
		// sinon on retourne une erreur
		return false;
	}

	// article du catalogue
	// dans la fonction, on met un arguments 'typo' car on cherche les caractéristiques de l'article
	public function getCatalogue ($typo) {
		// les données seront mises dans ce tableau
		$cat = [];
		// on ouvre le document dans lequel il y a l'argument que l'on cherche
		$h = fopen ('nodex/produits.csv', 'r');

		// dans ce docuemnt, on analyse le fichier ligne par ligne. le fichier est contenu dans $h, '0' pour dire qu'on prend tout le fichier (pas de limite) et le séparateur ';'
		while ($ligne = fgetcsv($h, 0, ';')) {
			// on décompose les typologies de client pour chaque produit. array_map applique une fonction sur un élément d'un tableau. 'trim' en elève les espaces; explode -> coupe une chaîne en segments; le séparateur ',' puis on extrait la 5èm" colonne
			$typos = array_map('trim', explode(',', $ligne[5]));

			// si la type donnée fait partie des typos pour un produit en cours
			// in_array -> indique si une valeur appartient à un tableau
			if (in_array($typo, $typos)){
				//on ajout le produit au catalogue
				$cat[] = new article($ligne);
			}
		}
		return $cat;
	}
}

?>