<?php
	// on crée une classe article, les attributs sont privés (diff de public pour pas les modifier)
	class article {
		private $ref;
		private $matiere;
		private $longueur;
		private $largeur;
		private $epaisseurs;

	// on crée une méthode et on lui dit où aller chercher les variable dans le tableau dans lequel il ira chercher les info "produit.csv"

	//
		public function __construct($array) {
		// l'objet $this avec l'opérateur -> puis le nom de la méthode à invoquer.
			$this->ref = $array[0];
			$this->matiere = $array[1];
			$this->longueur = $array[2];
			$this->largeur = $array[3];
			$this->epaisseurs = array_map('trim', explode(',', $array[4])) ;
		}

		// récupérer dans cette méthode les données propre aux objets. mettre dans un foreach les épaisseurs car il y en a plusieurs. puis mettre un bouton à la fin qui dirige sur la page couper.php
		public function rendreHtml() {
			$html = "<article><h3>{$this->matiere}</h3><h4>{$this->longueur} mm x {$this->largeur} mm</h4><select>";
			foreach ($this->epaisseurs as $epaisseur) {
				$html .= "<option value=\"$epaisseur\">$epaisseur mm</option>";
			}
			$html .= "</select><a href=\"couper.php\"></a></article>";
		}
		public function __toString(){
			return rendreHtml();
		}
	}
?>