<?php

session_start();

require 'classes/mapper.php';

$erreur = [];

if ($client = mapper::getClient($_POST['identifiant'], $_POST['mdp'])) {
	$_SESSION['client'] = $client;
	header('Localisation: page2.php');
	exit;
}else{
	$erreur = 'les identifiants ou mot de passe sont incorrects.';
}

$_SESSION['erreur'] = $erreur;

?>
