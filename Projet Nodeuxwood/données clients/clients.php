<?php

class client{
	private $prenom;
	private $nom;
	private $identifiant;
	private $typologie;

	public function __construct($array) {
		$this->prenom = $array[0];
		$this->nom = $array[1];
		$this->identifiant = $array[2];
		$this->typologie = $array[4];
	}

	public function getTypologie(){
		return $this->typologie;
	}
}

?>